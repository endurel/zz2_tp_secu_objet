import asyncio
import argparse
import sys
import math

from collections import namedtuple
from datetime import datetime

from struct import unpack, pack

from bleak import BleakClient, BleakScanner
from bleak.backends.characteristic import BleakGATTCharacteristic

from progress.bar import IncrementalBar

### Bar ###

class MyBar(IncrementalBar):
    message = 'Loading...'
    fill = '#'
    suffix = '%(percent).1f%% - %(eta)ds'

### Argument Parser ###

class MyArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

ap = MyArgumentParser()
ap.add_argument("-m", "--mac", help="mac address of the device to connect to")
ap.add_argument("-s", "--ssid", help="ssid of the device to connect")
args = vars(ap.parse_args())

if args.get("mac") == None and args.get("ssid") == None:
    ap.error("Argument non valide")

### Global ###

suffixe = "-a720-f7e9-46b6-31b601c4fca1"
prefixe = "1b0d"

list_uuid = prefixe + "1302" + suffixe
log_uuid = prefixe + "140a" + suffixe
ack_uuid = prefixe + "1407" + suffixe
read_uuid = prefixe + "1304" + suffixe

FsFileInfo = namedtuple('FsFileInfo', 'name size phash date')
Ack = namedtuple('Ack', 'carId err res pstr')
FileRequest = namedtuple('FileRequest', 'name chunkId numChunk')

files_list = dict()

### Functions ###

def hex_format(hexraw: bytearray):
    return ''.join(['{:02X}'.format(b) for b in hexraw])

async def discover():
    devices = await BleakScanner.discover()
    device = None
    for d in devices:
        if d.name == args.get("ssid") or d.address == args.get("mac"):
            device = d
            break
    return device

def print_services(client):
    services = client.services
    print("Services disponibles :")
    for service in services:
        s_name = f"{service}"[4:8]
        print(f"- {s_name}")
        characteristics = service.characteristics
        for characteristic in characteristics:
            c_sname = f"{characteristic}"[4:8]
            properties = characteristic.properties
            print(f"\t- {c_sname}: ( ", end="")
            for propertie in properties:
                print(f"{propertie}", end=" ")
            print(")")

def get_choice():
    print()
    print("-"*50)
    print("\nQue voulez-vous faire ?\n")
    print("\t[1] À propos")
    print("\t[2] Lister les services")
    print("\t[3] Lister les fichiers")
    print("\t[4] Lire un fichier")
    print("\t[5] Déconnexion\n")
    print("-"*50)
    return input("\nVotre choix : ")

def list_callback(verbose = True):
    def list_callback(sender: BleakGATTCharacteristic, data: bytearray):
        finfo = FsFileInfo._make(unpack("<8sIIQ", data))

        name_string = bytes.fromhex(hex_format(finfo.name))
        result = name_string.decode('utf-8').replace("\x00", "")

        files_list[result] = dict({
            "name": result,
            "size": finfo.size,
            "hash": finfo.phash,
            "date": finfo.date
        })

        if verbose is True:
            print(f"[LIST] {sender}: name = {result} size = {finfo.size} hash = {finfo.phash} date = {datetime.fromtimestamp(finfo.date / 1000)}")

    return list_callback

def log_callback(sender: BleakGATTCharacteristic, data: bytearray):
    print(f"[LOG ] {sender}: {data}")

def ack_callback(sender: BleakGATTCharacteristic, data: bytearray):
    ack_parse = Ack._make(unpack("<HHI20s", data))
    print(f"[ACK ] {sender}: carId = {ack_parse.carId} err = {ack_parse.err} res = {ack_parse.res} str = {hex_format(ack_parse.pstr)}")

def read_callback(file_write, bar):
    def read_callback(sender: BleakGATTCharacteristic, data: bytearray):
        # print(f"[READ] {sender}: {hex_format(data)[:50]}...")
        if file_write != None:
            file_write.write(data)
        bar.next()
    return read_callback
        
async def subscribe_notify(client):
    await client.start_notify(list_uuid, list_callback(verbose=True))
    await client.start_notify(log_uuid, log_callback)
    await client.start_notify(ack_uuid, ack_callback)

    print("Notify subscriptions done")

async def list_files(client):
    num = await client.read_gatt_char(prefixe + "1303" + suffixe)
    print(f"Nombres de fichiers: {int.from_bytes(num, 'little')}")
    await client.write_gatt_char(prefixe + "1302" + suffixe, num, response=True)

async def read_file(client):
    await client.stop_notify(ack_uuid)
    await client.stop_notify(log_uuid)

    name = input("Nom du fichier : ")
    name_b = name.encode('ascii') + bytearray(8 - len(name))
    
    try:
        file_write = open(name, "wb+")

        # cid = input("Début : ")
        cid = 0
        # cnum = input("Longueur : ")
        cnum = 9999
        request = pack("<8sII", name_b, int(cid), int(cnum))

        max_count = math.ceil(files_list[name]["size"] / 230)
        
        with MyBar("\tLoading...\t", max=max_count) as bar:
            await client.start_notify(read_uuid, read_callback(file_write, bar))
            await client.write_gatt_char(prefixe + "1304" + suffixe, request, response=True)

        while (bar.remaining != 0):
            await asyncio.sleep(0.1)
            
        file_write.close()

        await client.start_notify(ack_uuid, ack_callback)
        await client.start_notify(log_uuid, log_callback)

    except Exception as e:
        print("[ERR ] Impossible d'ouvrir le fichier", e)

async def treatment(client, choice):
    if (choice == "1"):
        print("Bonjour")
    elif (choice == "2"):
        print_services(client)
    elif (choice == "3"):
        await list_files(client)
    elif (choice == "4"):
        await read_file(client)
    elif (choice == "5"):
        print("Déconnexion en cours...")
        await client.disconnect()
        print("Déconnexion réussie")
    else:
        print("Choix invalide")

async def main(device):
    address = device.address
    name = device.name
    async with BleakClient(address) as client:
        # model_number = await client.read_gatt_char(MODEL_NBR_UUID)
        # print("Model Number: {0}".format("".join(map(chr, model_number))))
        print(f"Connecté à {name} ({address})")

        is_connected = client.is_connected
        await subscribe_notify(client)

        await list_files(client)

        await asyncio.sleep(2)

        # print(files_list)
        
        while is_connected:
            choice = get_choice()
            await treatment(client, choice)

            is_connected = client.is_connected

            await asyncio.sleep(1)

async def connect():
    device = await discover()
    if device == None:
        print("Impossible d'établir la connexion")
        exit(1)
    await main(device)

try:
    asyncio.run(connect())
except Exception as e:
    print("[ERR ]:" , e)
